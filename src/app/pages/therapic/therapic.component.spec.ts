import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TherapicComponent } from './therapic.component';

describe('TherapicComponent', () => {
  let component: TherapicComponent;
  let fixture: ComponentFixture<TherapicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TherapicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TherapicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
