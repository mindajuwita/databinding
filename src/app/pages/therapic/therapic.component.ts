import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-therapic',
  templateUrl: './therapic.component.html',
  styleUrls: ['./therapic.component.css']
})
export class TherapicComponent implements OnInit {

  constructor(config: NgbModalConfig, private modalService: NgbModal) { }

  ngOnInit() {
  	this.getData();
  }
dataSource:any=[];
  user:any={};

  getData()
  {
  		this.dataSource=[
  			{
  				nama_obat: 'ANALSPEC',
  				komposisi:'Tiap kaplet mengandung Asam mefenamat 500 mg'
  			},
  			{
  				nama_obat: 'ASAMNEX',
  				komposisi:'Tiap tablet salut selaput mengandung Tranexamic acid 500 mg'
  			},
  			{
  				nama_obat: 'BIOSAN',
  				komposisi:'Tiap tablet mengandung Ekstrak Tribulus Terrestris 275 mg'
  			},
			{
  				nama_obat: 'CEFTRIMET',
  				komposisi:'Ceftriaxone disodium sterile 1.194 gram, setara dengan Ceftriaxone 1 gram'
  			},
			{
  				nama_obat: 'KADIFLAM',
  				komposisi:'Tiap tablet salut selaput mengandung Diclofenac potassium 50 mg'
  			},



  		];
  }


  open(content) {
    this.modalService.open(content);
  }


  saveData(user)
  {
  	console.log(user);
  	this.dataSource.unshift(user);
  	//this.user={};
  }
 
  deleteData(idx)
  {
  	var konfirmasi=confirm('Yakin akan menghapus data?');
  	if(konfirmasi==true)
  	{
  		this.dataSource.splice(idx,1);
  	}
  }


}

}
