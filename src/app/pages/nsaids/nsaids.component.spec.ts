import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NsaidsComponent } from './nsaids.component';

describe('NsaidsComponent', () => {
  let component: NsaidsComponent;
  let fixture: ComponentFixture<NsaidsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NsaidsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NsaidsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
