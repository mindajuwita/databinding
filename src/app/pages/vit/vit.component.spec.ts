import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VitComponent } from './vit.component';

describe('VitComponent', () => {
  let component: VitComponent;
  let fixture: ComponentFixture<VitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
