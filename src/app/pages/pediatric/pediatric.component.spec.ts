import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PediatricComponent } from './pediatric.component';

describe('PediatricComponent', () => {
  let component: PediatricComponent;
  let fixture: ComponentFixture<PediatricComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PediatricComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PediatricComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
