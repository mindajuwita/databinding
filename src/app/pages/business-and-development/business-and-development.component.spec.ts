import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessAndDevelopmentComponent } from './business-and-development.component';

describe('BusinessAndDevelopmentComponent', () => {
  let component: BusinessAndDevelopmentComponent;
  let fixture: ComponentFixture<BusinessAndDevelopmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessAndDevelopmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessAndDevelopmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
