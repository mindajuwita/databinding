import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { OurProductsComponent } from './pages/our-products/our-products.component';
import { BusinessAndDevelopmentComponent } from './pages/business-and-development/business-and-development.component';
import { ContactUsComponent } from './pages/contact-us/contact-us.component';
import { RepresentativeComponent } from './pages/representative/representative.component';
import { PartnershipComponent } from './pages/partnership/partnership.component';
import { NsaidsComponent } from './pages/nsaids/nsaids.component';
import { VitComponent } from './pages/vit/vit.component';
import { TherapicComponent } from './pages/therapic/therapic.component';
import { ObygnComponent } from './pages/obygn/obygn.component';
import { PediatricComponent } from './pages/pediatric/pediatric.component';
import { SurgeryComponent } from './pages/surgery/surgery.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    OurProductsComponent,
    BusinessAndDevelopmentComponent,
    ContactUsComponent,
    RepresentativeComponent,
    PartnershipComponent,
    NsaidsComponent,
    VitComponent,
    TherapicComponent,
    ObygnComponent,
    PediatricComponent,
    SurgeryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
	FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
