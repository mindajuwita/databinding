import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { OurProductsComponent } from './pages/our-products/our-products.component';
import { BusinessAndDevelopmentComponent } from './pages/business-and-development/business-and-development.component';
import { ContactUsComponent } from './pages/contact-us/contact-us.component';
import { RepresentativeComponent } from './pages/representative/representative.component';
import { PartnershipComponent } from './pages/partnership/partnership.component';
import { NsaidsComponent } from './pages/nsaids/nsaids.component';
import { VitComponent } from './pages/vit/vit.component';
import { TherapicComponent } from './pages/therapic/therapic.component';
import { ObygnComponent } from './pages/obygn/obygn.component';
import { PediatricComponent } from './pages/pediatric/pediatric.component';
import { SurgeryComponent } from './pages/surgery/surgery.component';

const routes: Routes = [
{
path:'home',
component:HomeComponent
},
{
path:'our-products',
component:OurProductsComponent
},
{
path:'business-and-development',
component:BusinessAndDevelopmentComponent
},
{
path:'contact-us',
component:ContactUsComponent
},
{
path:'representative',
component:RepresentativeComponent
},
{
path:'partnership',
component:PartnershipComponent
},
{
path:'nsaids',
component:NsaidsComponent
},
{
path:'vit',
component:VitComponent
},
{
path:'therapic',
component:TherapicComponent
},
{
path:'obygn',
component:ObygnComponent
},
{
path:'pediatric',
component:PediatricComponent
},
{
path:'surgery',
component:SurgeryComponent
}



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
